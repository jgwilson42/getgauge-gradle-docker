# Copyright Diffblue Ltd. 2018-2019. All rights reserved.
FROM gradle:5.6.2-jdk8
RUN curl -SsL https://downloads.gauge.org/stable | sh
RUN gauge install html-report
RUN gauge install java
