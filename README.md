# Docker image with Gauge and Gradle

A docker image (hosted on DockerHub) that includes both Gauge (https://getgauge.io) and Gradle. Used to build projects that require Gauge and Gradle.
